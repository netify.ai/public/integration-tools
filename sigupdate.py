import hashlib
import json
import os
import requests

def main():
    api_key='sk_xx_live_xxxxxxxxxxxxxxxxxxxxxxxx'
    applications_file='/etc/netify.d/netify-apps.conf'
    categories_file='/etc/netify.d/netify-categories.json'
    applications_tempfile='/tmp/netify.sig'
    categories_tempfile='/tmp/netify.cat'
    url='https://informatics.netify.ai/api/v2/lookup/signatures/applications'
    version='4'

    params = {
        'settings_version': version,
        'settings_format': 'netifyd',
    }

    headers = {
        'content-type': 'application/json',
        'x-api-key': api_key,
    }

    response = requests.get(url, data=json.dumps(params), headers=headers)

    if (response.status_code != 200):
        print('Failed to download application signatures: ' + response.reason)
        return

    if not 'X-SHA256-Hash' in response.headers:
        print('Could not verify file integrity')
        return

    sha256_temp = response.headers['X-SHA256-Hash']

    if not sha256_temp:
        print('Unable to verify SHA256 hash')
        return

    # Update may not be required
    with open(applications_file,'rb') as f:
        bytes = f.read()
        sha256_current = hashlib.sha256(bytes).hexdigest()
        if sha256_temp == sha256_current:
            print('No update required')
            return

    with open(applications_tempfile, 'wb') as f:
        f.write(response.content)
    f.close()

    with open(applications_tempfile,'rb') as f:
        bytes = f.read()
        sha256_actual = hashlib.sha256(bytes).hexdigest()
        if sha256_temp != sha256_actual:
            print('Unable to verify SHA256 hash')
            return

    # We know we need an update, get the latest categories
    url='https://informatics.netify.ai/api/v2/lookup/signatures/categories'

    response = requests.get(url, data=json.dumps(params), headers=headers)

    if (response.status_code != 200):
        print('Failed to download categories: ' + response.reason)
        return

    with open(categories_tempfile, 'wb') as f:
        f.write(response.content)
    f.close()

    # We don't have file integrity on category file...we should

    os.replace(categories_tempfile, categories_file)
    os.replace(applications_tempfile, applications_file)
    os.system('killall -HUP netifyd')
    print('OK')

if __name__ == '__main__':
    main()
