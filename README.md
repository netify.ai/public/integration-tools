# Integration Tools

## C (C99)

Example written in C (C99).  See README in subfolder.

## sigupdate.*

Python and bash sample scripts for pulling signature and category updates using a Netify supplied vendor API key.

## feed.py

A Python script for subscribers of the Netify Data Feed to explore the data structure and view ways to manipulate (ex. creating CSV files).

Calling the script requires at least two arguments (application and type of data feed).

### Application

The application of interest that can be the ID or tagname.  Application list can be obtained using:

    curl https://informatics.netify.ai/api/v2/lookup/applications?settings_limit=5000&settings_show_default_logo=false 

### Type

Specifying the type changes the API endpoint accessed and the data structure in the response.  Valid types are:

* domains
* ips
* stats

#### Domains

    python feed.py reddit domains

#### IP Addresses 

    python feed.py reddit ips

#### Stats

    python feed.py reddit stats 


### CSV

The default payload returns the raw data returned from the RESTful API call that uses JSON.  Calling the script with a third argument specifying 'csv' will
demonstrate how to convert JSON to CSV format.

Calling the script with the csv argument returns CSV formatted data that is by no means exhaustive.  It selects a few attributes and exports (via stout
redirection) in CSV format.  The intent is to show subscribers of the Netify Data Feed how to manipulate the data programmatically.

Example:

    python feed.py reddit domains csv
