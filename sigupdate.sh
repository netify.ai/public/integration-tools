#!/bin/bash
AUTH='x-api-key: sk_xx_live_xxxxxxxxxxxxxxxxxxxx'
curl -s --header "$AUTH" \
    "https://informatics.netify.ai/api/v2/lookup/signatures/applications?settings_version=4&settings_format=netifyd" \
    --output "/tmp/netify.sig"

curl -s --header "$AUTH" \
    "https://informatics.netify.ai/api/v2/lookup/signatures/categories?settings_version=4&settings_format=netifyd" \
    --output "/tmp/netify.cat"

mv /tmp/netify.sig /etc/netify.d/netify-apps.conf
mv /tmp/netify.cat /etc/netify.d/netify-categories.json

killall -HUP netifyd
