import json
import sys
import requests
import getopt

API_KEY=''

def main(argv):
    if len(argv) < 3:
        print ('feed.py <application> <feed type> csv (optional)')
        sys.exit(2)

    application = argv[1]
    feedType = argv[2]

    csv = False
    if feedType == 'domains':
        endpoint='https://feeds.netify.ai/api/v2/intelligence/application_domains/' + application
    elif feedType == 'stats':
        endpoint = 'https://feeds.netify.ai/api/v2/stats/applications/' + application
    elif feedType == 'ips':
        endpoint='https://feeds.netify.ai/api/v2/intelligence/application_ips/' + application
    else:
        print ('Feed type can be domains, ips or stats')
        sys.exit(2)

    if len(argv) == 4 and argv[3] == 'csv':
        csv = True

    url = 'https://informatics.netify.ai/api/v2/lookup/applications'
    params = {}

    headers = {
        'content-type': 'application/json',
        'x-api-key': API_KEY,
    }

    response = requests.get(endpoint, data=json.dumps(params), headers=headers)

    if response.status_code != 200:
        print(response.json()['status_message'])
        sys.exit(2)

    data = response.json()['data']

    if csv == True:
        if feedType == 'domains':
            print("\"Domain\",\"Category\",\"Platform\",\"Platform Category\"")
            for d in data['domain_list']:
                print("\"" + d['label'] + "\"," +
                    "\"" + (d['category']['label'] if 'category' in d else "") + "\"," +
                    "\"" + (d['platform']['label'] if 'platform' in d else "") + "\"," +
                    "\"" + (d['platform']['category']['label'] if 'platform' in d else "") + "\""
                )
        elif feedType == 'ips':
            print("\"IP\",\"ASN\",\"ASN Platform\",\"Platform Category\"")
            for d in data['active_ip_list']:
                print(
                    "\"" + d['address'] + "\"," +
                    "\"" + (d['asn']['tag'] if 'asn' in d else "") + "\"," +
                    "\"" + (d['asn']['label'] if 'asn' in d else "") + "\""
                )
        elif feedType == 'stats':
            print("\"IP\",\"Hostname\"")
            for d in data['stats_ip_list']:
                for i in d['hostname_stats']:
                    print(
                        "\"" + d['ip']['address'] + "\"," +
                        "\"" + (i['hostname_info']['hostname'] if 'hostname_info' in i else "") + "\""
                    )

    if csv == False:
        jprint(data)

def jprint(obj):
    text = json.dumps(obj, sort_keys=True, indent=4)
    print(text)

if __name__ == '__main__':
    main(sys.argv)
