/* Netify Data Feeds API C99 Example
 *
 * https://www.netify.ai/developer/netify-data-feeds
 * https://gitlab.com/netify.ai/public/netify-api-example-c99
 *
 * Requirements:
 *
 * - libcurl: https://curl.se/libcurl/
 * - libjson-c: https://json-c.github.io/json-c/
 *
 * To use the included Makefile, pkg-config is required.
 */

#define _GNU_SOURCE

#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>
#include <string.h>
#include <strings.h>
#include <errno.h>
#include <getopt.h>

#include <sys/time.h>

#include <curl/curl.h>
#include <json.h>

#define _ND_MAX_REQUESTS        10
#define _ND_FILENAME_SIZE       1024

typedef struct nd_example_ctx_t
{
    bool debug;
    bool tls_verify;
    CURL *ch;
    struct curl_slist *headers;
    const char *tag;
    char *debug_buffer;
    size_t debug_buffer_size;
    uint8_t *data;
    uint8_t *data_offset;
    size_t data_size;
    const char *request_url;
    const char *api_key;
    char *url;
    char *params;
    const char *save_prefix;
    time_t save_stamp;
} nd_example_ctx;

static void nd_ctx_free(nd_example_ctx *ctx)
{
    if (ctx->ch != NULL)
        curl_easy_cleanup(ctx->ch);

    if (ctx->headers != NULL)
        curl_slist_free_all(ctx->headers);

    if (ctx->debug_buffer != NULL)
        free(ctx->debug_buffer);

    if (ctx->data != NULL)
        free(ctx->data);

    if (ctx->url != NULL)
        free(ctx->url);

    if (ctx->params != NULL)
        free(ctx->params);

    memset(ctx, 0, sizeof(struct nd_example_ctx_t));
}

static void nd_ctx_reset(nd_example_ctx *ctx)
{
    ctx->data_size = 0;
    ctx->data_offset = ctx->data;
}

static int nd_curl_debug(CURL *ch __attribute__((unused)),
    curl_infotype type, char *data, size_t size, void *user)
{
    nd_example_ctx *ctx = (nd_example_ctx *)user;

    if (! ctx->debug || ! size) return 0;

    if (size + 1 > ctx->debug_buffer_size) {
        ctx->debug_buffer_size = size + 1;
        ctx->debug_buffer = realloc(
            ctx->debug_buffer, ctx->debug_buffer_size
        );

        if (ctx->debug_buffer == NULL) {
            fprintf(stderr, "%s: realloc: %s\n",
                __PRETTY_FUNCTION__, strerror(errno)
            );

            return 0;
        }
    }

    ctx->debug_buffer[size] = '\0';

    switch (type) {
    case CURLINFO_TEXT:
        strncpy(ctx->debug_buffer, data, size);
        fprintf(stderr, "%s: %s", ctx->tag, ctx->debug_buffer);
        break;
    case CURLINFO_HEADER_IN:
        strncpy(ctx->debug_buffer, data, size);
        fprintf(stderr, "%s: <-- %s", ctx->tag, ctx->debug_buffer);
        break;
    case CURLINFO_HEADER_OUT:
        strncpy(ctx->debug_buffer, data, size);
        fprintf(stderr, "%s: --> %s", ctx->tag, ctx->debug_buffer);
        break;
    case CURLINFO_DATA_IN:
        fprintf(stderr, "%s: <-- %lu data bytes\n", ctx->tag, size);
        break;
    case CURLINFO_DATA_OUT:
        fprintf(stderr, "%s: --> %lu data bytes\n", ctx->tag, size);
        break;
    case CURLINFO_SSL_DATA_IN:
        fprintf(stderr, "%s: <-- %lu SSL bytes\n", ctx->tag, size);
        break;
    case CURLINFO_SSL_DATA_OUT:
        fprintf(stderr, "%s: --> %lu SSL bytes\n", ctx->tag, size);
        break;
    default:
        break;
    }

    return 0;
}

static size_t nd_curl_read_data(
    char *data, size_t size, size_t nmemb, void *user)
{
    size_t length = size * nmemb;

    if (length == 0) return length;

    nd_example_ctx *ctx = (nd_example_ctx *)user;

    ctx->data = realloc(ctx->data, ctx->data_size + length);

    if (ctx->data == NULL) {
        fprintf(stderr, "%s: realloc(data): %s\n",
            __PRETTY_FUNCTION__, strerror(errno)
        );

        return 0;
    }

    ctx->data_offset = ctx->data + ctx->data_size;
    ctx->data_size += length;

    if (ctx->debug) {
        fprintf(stderr, "data: %p, offset: %p, length: %lu\n",
            ctx->data, ctx->data_offset, ctx->data_size);
    }

    memcpy(ctx->data_offset, data, length);

    return length;
}

static int nd_parse_response(nd_example_ctx *ctx, unsigned *pages)
{
    enum json_tokener_error jerr;
    struct json_object *jobj = json_tokener_parse_verbose(
        (const char *)ctx->data, &jerr
    );

    if (jobj == NULL) {
        fprintf(stderr, "%s: parse error: %s\n",
            ctx->tag, json_tokener_error_desc(jerr)
        );
        return EXIT_FAILURE;
    }

    struct json_object *jcode = NULL;
    json_object_object_get_ex(jobj, "status_code", &jcode);

    if (jcode == NULL) {
        fprintf(stderr, "%s: status_code not found.\n", ctx->tag);
        goto nd_parse_response_error;
    }

    int32_t code = json_object_get_int(jcode);

    struct json_object *jmessage = NULL;
    json_object_object_get_ex(jobj, "status_message", &jmessage);

    if (jmessage == NULL) {
        fprintf(
            stderr, "%s: status_message not found.\n", ctx->tag
        );
        goto nd_parse_response_error;
    }

    const char *message = json_object_get_string(jmessage);
    fprintf(stderr,
        "%s: API response code: %d, message: %s\n",
        ctx->tag, code, message
    );

    if (code == 422) { /* Validation error */
        struct json_object *jfields = NULL;
        json_object_object_get_ex(jobj, "status_fields", &jfields);

        json_object_object_foreach(jfields, key, value) {
            fprintf(stderr, "%s: %s: %s\n",
                ctx->tag,
                key,
                json_object_get_string(value)
            );
        }
    }

    if (code != 0 && code != 200)
        goto nd_parse_response_error;

    unsigned page = 0;
    struct json_object *jdata_info = NULL;
    json_object_object_get_ex(jobj, "data_info", &jdata_info);

    if (jdata_info != NULL) {
        struct json_object *jpage = NULL;
        json_object_object_get_ex(jdata_info, "current_page", &jpage);

        if (jpage == NULL) {
            fprintf(
                stderr, "%s: current_page not found.\n", ctx->tag
            );
            goto nd_parse_response_error;
        }

        page = (unsigned)json_object_get_int(jpage);

        struct json_object *jpages = NULL;
        json_object_object_get_ex(jdata_info, "total_pages", &jpages);

        if (jpages == NULL) {
            fprintf(
                stderr, "%s: total_pages not found.\n", ctx->tag
            );
            goto nd_parse_response_error;
        }

        *pages = (unsigned)json_object_get_int(jpages);
    }

    if (ctx->save_prefix != NULL) {
        char filename[_ND_FILENAME_SIZE];
        snprintf(filename, _ND_FILENAME_SIZE,
            "%s-%lu-%03d-%03d.json",
            ctx->save_prefix, ctx->save_stamp,
            page, *pages
        );

        FILE *fh = fopen(filename, "w+");
        if (fh == NULL) {
            fprintf(stderr, "%s: fopen(%s): %s\n",
                ctx->tag, filename, strerror(errno)
            );
        }
        else {
            fwrite(ctx->data, 1, ctx->data_size, fh);
            fclose(fh);

            fprintf(stderr, "%s: wrote result to: %s\n",
                ctx->tag, filename
            );
        }
    }

    if (jobj != NULL) json_object_put(jobj);
    return EXIT_SUCCESS;

nd_parse_response_error:
    if (jobj != NULL) json_object_put(jobj);
    return EXIT_FAILURE;
}

static int nd_api_request(nd_example_ctx *ctx)
{
    const char *content_type = NULL;
    CURLcode curl_rc;
    double content_length = 0.0f;
    long http_rc = 0;
    unsigned page = 1, pages = 0;

    if (ctx == NULL) {
        fprintf(stderr,
            "%s: invalid context.\n", __PRETTY_FUNCTION__
        );
        return EXIT_FAILURE;
    }

    if (ctx->request_url == NULL) {
        fprintf(stderr, "%s: request-url must be set.\n", ctx->tag);
        return EXIT_FAILURE;
    }

    if (ctx->ch == NULL) {
        ctx->ch = curl_easy_init();

        if (ctx->ch == NULL) {
            fprintf(stderr, "%s: curl_easy_init: %s\n", ctx->tag,
                "unable to initialize"
            );
            return EXIT_FAILURE;
        }

        curl_easy_setopt(ctx->ch, CURLOPT_MAXREDIRS, 3);
        curl_easy_setopt(ctx->ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_easy_setopt(ctx->ch, CURLOPT_CONNECTTIMEOUT, 20);
        curl_easy_setopt(ctx->ch, CURLOPT_TIMEOUT, 60);

        curl_easy_setopt(ctx->ch, CURLOPT_WRITEFUNCTION, nd_curl_read_data);
        curl_easy_setopt(ctx->ch, CURLOPT_WRITEDATA, (void *)ctx);

#if (LIBCURL_VERSION_NUM >= 0x072106)
        curl_easy_setopt(ctx->ch, CURLOPT_ACCEPT_ENCODING, "gzip");
#endif
        if (ctx->debug) {
            curl_easy_setopt(ctx->ch, CURLOPT_VERBOSE, 1);
            curl_easy_setopt(ctx->ch, CURLOPT_DEBUGFUNCTION, nd_curl_debug);
            curl_easy_setopt(ctx->ch, CURLOPT_DEBUGDATA, (void *)ctx);
        }

        if (ctx->tls_verify == false) {
            curl_easy_setopt(ctx->ch, CURLOPT_SSL_VERIFYPEER, 0);
            curl_easy_setopt(ctx->ch, CURLOPT_SSL_VERIFYHOST, 0);
        }

        if (ctx->api_key != NULL && ctx->headers == NULL) {
#define _ND_APIKEY_HEADER       "X-API-Key: "
#define _ND_APIKEY_HEADER_LEN   (sizeof(_ND_APIKEY_HEADER))
            const size_t header_length = _ND_APIKEY_HEADER_LEN
                + strlen(ctx->api_key);
            char *header = malloc(header_length);

            if (header == NULL) {
                fprintf(stderr, "%s: malloc(header): %s\n",
                    ctx->tag, strerror(errno)
                );
                return EXIT_FAILURE;
            }

            snprintf(
                header,
                header_length,
                "%s%s",
                _ND_APIKEY_HEADER, ctx->api_key
            );

            ctx->headers = curl_slist_append(
                ctx->headers,
                header
            );

            free(header);

            curl_easy_setopt(
                ctx->ch, CURLOPT_HTTPHEADER, ctx->headers
            );
        }
    }

    do {
        struct timeval tv_start = { 0 }, tv_end = { 0 };
        gettimeofday(&tv_start, NULL);

#define _ND_PAGE_PARAM              "?page="
#define _ND_PAGE_PARAM_LEN          (sizeof(_ND_PAGE_PARAM))
        const int page_length = snprintf(NULL, 0, "%u", page);

        const size_t params_length =
            _ND_PAGE_PARAM_LEN + page_length;

        ctx->params = realloc(ctx->params, params_length);

        if (ctx->params == NULL) {
            fprintf(stderr, "%s: realloc(params): %s\n",
                ctx->tag, strerror(errno)
            );
            return EXIT_FAILURE;
        }

        snprintf(
            ctx->params, params_length, "%s%u",
            _ND_PAGE_PARAM, page
        );

        const size_t url_length = params_length
            + strlen(ctx->request_url);

        ctx->url = realloc(ctx->url, url_length);

        if (ctx->url == NULL) {
            fprintf(stderr, "%s: realloc(url): %s\n",
                ctx->tag, strerror(errno)
            );
            return EXIT_FAILURE;
        }

        snprintf(
            ctx->url, url_length,
            "%s%s", ctx->request_url, ctx->params
        );

        fprintf(stderr, "%s: sending API request: %s\n",
            ctx->tag, ctx->url
        );

        curl_easy_setopt(ctx->ch, CURLOPT_URL, ctx->url);

        if ((curl_rc = curl_easy_perform(ctx->ch)) != CURLE_OK) {
            fprintf(stderr, "%s: curl_easy_perform: %s\n",
                ctx->tag, curl_easy_strerror(curl_rc)
            );
            return EXIT_FAILURE;
        }

        if ((curl_rc = curl_easy_getinfo(ctx->ch,
            CURLINFO_RESPONSE_CODE, &http_rc)) != CURLE_OK) {
            fprintf(
                stderr,
                "%s: curl_easy_getinfo(response_code): %s\n",
                ctx->tag, curl_easy_strerror(curl_rc)
            );
            return EXIT_FAILURE;
        }

        if (http_rc != 200) {
            fprintf(stderr, "%s: unexpected response code: %lu\n",
                ctx->tag, http_rc
            );
        }

        curl_easy_getinfo(
            ctx->ch, CURLINFO_CONTENT_TYPE, &content_type
        );

        if (content_type == NULL
            || strcasecmp(content_type, "application/json")) {

            fprintf(stderr, "%s: invalid content type.\n",
                ctx->tag
            );

            if (ctx->debug && content_type != NULL) {
                fprintf(stderr, "%s: content type: \"%s\"\n",
                    ctx->tag, content_type
                );
            }

            return EXIT_FAILURE;
        }

        curl_easy_getinfo(
            ctx->ch,
            CURLINFO_CONTENT_LENGTH_DOWNLOAD, &content_length
        );

        if (content_length == 0) {
            fprintf(stderr,
                "%s: WARNING: not content length set.\n",
                ctx->tag
            );
        }

        if (nd_parse_response(ctx, &pages) != EXIT_SUCCESS)
            return EXIT_FAILURE;

        nd_ctx_reset(ctx);

        gettimeofday(&tv_end, NULL);

        double seconds = (double)tv_end.tv_sec +
            ((double)tv_end.tv_usec / 1000000.0);
        seconds -= (double)tv_start.tv_sec +
            ((double)tv_start.tv_usec / 1000000.0);

        fprintf(stderr,
            "%s: processed API request for page %d/%d in %.02f second(s).\n",
            ctx->tag, page, (pages > 0) ? pages : 1, seconds
        );
    } while (++page <= pages);

    return EXIT_SUCCESS;
}

static void nd_example_usage(const char *name)
{
    fprintf(stderr, "Netify Data Feeds API C99 Example\n");
    fprintf(stderr, "https://www.netify.ai/developer/netify-data-feeds\n");
    fprintf(stderr, "https://gitlab.com/netify.ai/public/netify-api-example-c99\n\n");
    fprintf(stderr, "Parameters and arguments for: %s\n", name);
    fprintf(stderr, "  -d, --debug\n"
        "\tEnable debug mode.\n");
    fprintf(stderr, "  -r <url>, --request-url <url>"
        " [-r <url>, --request-url <url>] ...\n"
        "\tAt least one API request URL, maximum of %d.\n",
        _ND_MAX_REQUESTS
    );
    fprintf(stderr, "  -a <key>, --api-key <key>\n"
        "\tThe Netify Data Feeds API Key to use.\n");
    fprintf(stderr, "  -s <prefix>, --save <prefix>\n"
        "\tSave results to JSON file with filename <prefix>.\n");
}

int main(int argc, char *argv[])
{
    int rc;

    static nd_example_ctx ctx = {
        .debug = false,
        .tls_verify = true,
        .ch = NULL,
        .headers = NULL,
        .tag = "nd-api-example",
        .debug_buffer = NULL,
        .debug_buffer_size = 0,
        .data = NULL,
        .data_offset = 0,
        .data_size = 0,
        .request_url = NULL,
        .api_key = NULL,
        .url = NULL,
        .params = NULL,
        .save_prefix = NULL
    };

    static struct option options[] = {
        { "debug", 0, 0, 'd' },
        { "tls-no-verify", 0, 0, 'n' },
        { "help", 0, 0, 'h' },
        { "request-url", 1, 0, 'r' },
        { "api-key", 1, 0, 'a' },
        { "save", 1, 0, 's' },
        { NULL, 0, 0, 0 }
    };

    int request = 0;
    static const char *request_urls[_ND_MAX_REQUESTS] = { NULL };

    while (true) {
        if ((rc = getopt_long(argc, argv,
            "?dhnr:a:l:s:",
            options, NULL)) == -1) break;

        switch (rc) {
        case 0:
            break;
        case 'd':
            ctx.debug = true;
            break;
        case 'n':
            ctx.tls_verify = false;
            break;
        case 'r':
            if (request < _ND_MAX_REQUESTS)
                request_urls[request++] = optarg;
            else {
                fprintf(
                    stderr,
                    "%s: maximum requests reached (%d)\n",
                    ctx.tag, _ND_MAX_REQUESTS
                );
            }
            break;
        case 'a':
            ctx.api_key = optarg;
            break;
        case 's':
            ctx.save_prefix = (const char *)strndup(
                optarg, _ND_FILENAME_SIZE - 24
            );
            if (ctx.save_prefix == NULL) {
                fprintf(stderr, "%s: strndup(save_prefix): %s\n",
                    ctx.tag, strerror(errno)
                );
                return EXIT_FAILURE;
            }
            break;
        case '?':
            fprintf(stderr, "\n");
        case 'h':
        default:
            nd_example_usage(argv[0]);
            return EXIT_FAILURE;
        }
    }

    if (request == 0) {
        fprintf(stderr,
            "%s: At lease one --request-url required.\n\n", ctx.tag
        );
        nd_example_usage(argv[0]);
        return EXIT_FAILURE;
    }

    curl_global_init(CURL_GLOBAL_DEFAULT);

    for (
        request = 0;
        request < _ND_MAX_REQUESTS && request_urls[request] != NULL;
        request++) {

        ctx.save_stamp = time(NULL);
        ctx.request_url = request_urls[request];

        if ((rc = nd_api_request(&ctx) != EXIT_SUCCESS)) break;
    }

    nd_ctx_free(&ctx);
    curl_global_cleanup();

    return rc;
}
