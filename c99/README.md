# Netify Data Feeds API C99 Example

## Overview

This is a simple example demonstrating how to call the [Netify Data Feeds API](https://www.netify.ai/developer/netify-data-feeds) in C (C99) using libCURL and JSON-C.

## Requirements

The example code requires a C99 compiler and the following libraries:
- libcurl: https://curl.se/libcurl/
- libjson-c: https://json-c.github.io/json-c/

The included Makefile will automatically look for these dependencies using pkg-config.

## Usage

The executable accepts the following parameters and arguments:

- `-d, --debug`: Enable debug mode.
- `-r <url>, --request-url <url> [-r <url>, --request-url <url>] ...`: At least one API request URL.
- `-a <key>, --api-key <key>`: The Netify Data Feeds API Key to use.
- `-s <prefix>, --save <prefix>`: Save results to a JSON file using the specified path `<prefix>`.
