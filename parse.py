#!/usr/bin/python3 -Wi

import matplotlib.pyplot as plt
import json
import requests
import os
import sys
import time
import getopt

APP_FILE = 'applications.json'
URL = 'https://informatics.netify.ai/api/v2/lookup/applications'
INTERVAL = 15
SOURCE_FILE = './netify.json'

def usage():
    print("Usage: parse.py OPTIONS")
    print("")
    print("--source         Specify the source file (default is %s" %(SOURCE_FILE))
    print("--csv            Write output to CSV file (output_YYMMDD-HHMM.csv)")
    print("")

def main():

    # Defaults
    source_file = SOURCE_FILE
    csv = False

    try:
        opts, args = getopt.getopt(sys.argv[1:], "h", ["help", "csv", "source="])
    except getopt.GetoptError as err:
        print(err)
        sys.exit(2)
    for o, a in opts:
        if o in ("-h", "--help"):
            usage()
            sys.exit()
        elif o == "--csv":
            csv = True
        elif o in ("-o", "--source"):
            source_file = a

    app_list = applications()

    interval = 0 
    interval_bytes_dn = 0
    interval_bytes_up = 0
    first_seen = 0 
    last_seen = 0
    total_bytes_dn = 0
    total_bytes_up = 0
    purge_total_bytes = 0
    mapping = {}
    parent = {}
    bps_dn = []
    bps_up = []
    stats_entries = 0
    purge_entries = 0
    parent['netify.established'] = {'label': 'Established', 'bytes_dn': 0, 'bytes_up': 0}

    for opt, arg in opts:
        if opt == '-f':
            source_file = arg
        elif opt == '-csv':
            cvv = True

    with open (source_file) as file:
        for line in file:
            j = json.loads(line)
            if not 'length' in j:
                print("Error: length header not found.\n")
                os.exit(1)

            buffer = file.read(j['length'])
            j = json.loads(buffer)
            if ('type' in j and j['type'] == 'flow'):
                app = {'tag': 'netify.unknown', 'label': 'Unknown'}
                id = j['flow']['detected_application']
                if app_list.get(int(id)) is not None:
                    app = app_list.get(int(id))

                mapping[j['flow']['digest']] = {'application': app}

                if app['tag'] not in parent:
                    parent[app['tag']] = {'label': app['label'], 'bytes_dn': 0, 'bytes_up': 0}
            if ('type' in j and (j['type'] == 'flow_stats' or j['type'] == 'flow_purge')):
                if interval == 0: 
                    interval = j['flow']['last_seen_at'] / 1000

                if (interval + INTERVAL) > j['flow']['last_seen_at'] / 1000:
                    if (j['flow']['other_bytes'] > 0 or j['flow']['local_bytes'] > 0):
                        if interval_bytes_dn > 0:
                            bps_dn.append(round(interval_bytes_dn / INTERVAL, 1))
                        if interval_bytes_up > 0:
                            bps_up.append(round(interval_bytes_up / INTERVAL, 1))
                    interval_bytes_dn = 0
                    interval_bytes_up = 0
                    interval = j['flow']['last_seen_at'] / 1000
                
                if first_seen == 0 or first_seen > j['flow']['last_seen_at'] / 1000:
                    first_seen = int(j['flow']['last_seen_at'] / 1000)
                if last_seen < j['flow']['last_seen_at'] / 1000:
                    last_seen = int(j['flow']['last_seen_at'] / 1000)
                total_bytes_dn += j['flow']['other_bytes']
                total_bytes_up += j['flow']['local_bytes']
                interval_bytes_dn += j['flow']['other_bytes']
                interval_bytes_up += j['flow']['local_bytes']
                if ('type' in j and j['type'] == 'flow_purge'):
                    purge_total_bytes += j['flow']['total_bytes']
                    purge_entries+= 1
                else:
                    stats_entries+= 1
                if (j['flow']['digest'] in mapping): 
                    parent[mapping[j['flow']['digest']]['application']['tag']]['bytes_dn'] += j['flow']['other_bytes'] 
                    parent[mapping[j['flow']['digest']]['application']['tag']]['bytes_up'] += j['flow']['local_bytes'] 
                else:
                    parent['netify.established']['bytes_dn'] += j['flow']['other_bytes']
                    parent['netify.established']['bytes_up'] += j['flow']['local_bytes']
        if interval_bytes_dn > 0:
            bps_dn.append(round(interval_bytes_dn / INTERVAL, 1))
        if interval_bytes_up > 0:
            bps_up.append(round(interval_bytes_up / INTERVAL, 1))

    if csv:
        csv_file = 'output_' + time.strftime("%Y%m%d-%H%M") + '.csv'
        with open(csv_file, 'w') as output:
            output.write("\"Application\",\"Download (Bytes)\",\"Upload (Bytes)\"\n")
            for p in parent:
                output.write("\"" + parent[p]['label'] + "\",\"" + str(parent[p]['bytes_dn']) + "\",\"" + str(parent[p]['bytes_up']) + "\"\n")

            output.write("\"Total download bytes\",\"" + str(total_bytes_dn) + "\"\n")
            output.write("\"Total upload bytes\",\"" + str(total_bytes_up) + "\"\n")
            output.write("\"Total bytes\",\"" + str(total_bytes_dn + total_bytes_up) + "\"\n")
            output.write("\"Total purge bytes\",\"" + str(purge_total_bytes) + "\"\n")
            output.write("\"Stats entries\",\"" + str(stats_entries) + "\"\n")
            output.write("\"Purge entries\",\"" + str(purge_entries) + "\"\n")
            output.write("\"First seen\",\"" + str(first_seen) + "\"\n")
            output.write("\"Last seen\",\"" + str(last_seen) + "\"\n")
            output.write("\"Interval (seconds)\",\"" + str(last_seen - first_seen) + "\"\n")
            if len(bps_dn):
                output.write("\"Min download Bps\",\"" + str(int(min(bps_dn))) + "\"\n")
                output.write("\"Max download Bps\",\"" + str(int(max(bps_dn))) + "\"\n")
                output.write("\"Avg download Bps\",\"" + str(int(sum(bps_dn) / len(bps_dn))) + "\"\n")
            if len(bps_up):
                output.write("\"Min upload Bps\",\"" + str(int(min(bps_up))) + "\"\n")
                output.write("\"Max upload Bps\",\"" + str(int(max(bps_up))) + "\"\n")
                output.write("\"Avg upload Bps\",\"" + str(int(sum(bps_up) / len(bps_up))) + "\"\n")
        print('CSV file: ' + csv_file)
    else:
        for p in parent:
            print(parent[p]['label'])
            print('   Download bytes:       ' + str(parent[p]['bytes_dn']).rjust(19, ' '))
            print('   Upload bytes:         ' + str(parent[p]['bytes_up']).rjust(19, ' '))

        print('--------------------------------------------')
        print('Total download bytes:       ' + str(total_bytes_dn).rjust(16, ' '))
        print('Total upload bytes:         ' + str(total_bytes_up).rjust(16, ' '))
        print('Total bytes:                ' + str(total_bytes_dn + total_bytes_up).rjust(16, ' '))
        print('Purge total bytes:          ' + str(purge_total_bytes).rjust(16, ' '))
        print('Stats entries:              ' + str(stats_entries).rjust(16, ' '))
        print('Purge entries:              ' + str(purge_entries).rjust(16, ' '))
        print('--------------------------------------------')
        print('First seen:                 ' + str(first_seen).rjust(16, ' '))
        print('Last seen:                  ' + str(last_seen).rjust(16, ' '))
        print('Interval (seconds):         ' + str(last_seen - first_seen).rjust(16, ' '))
        if len(bps_dn):
            print('--------------------------------------------')
            print('Min download Bps:           ' + str(int(min(bps_dn))).rjust(16, ' '))
            print('Max download Bps:           ' + str(int(max(bps_dn))).rjust(16, ' '))
            print('Avg download Bps:           ' + str(int(sum(bps_dn) / len(bps_dn))).rjust(16, ' '))
        print('--------------------------------------------')
        if len(bps_up):
            print('Min upload Bps:             ' + str(int(min(bps_up))).rjust(16, ' '))
            print('Max upload Bps:             ' + str(int(max(bps_up))).rjust(16, ' '))
            print('Avg upload Bps:             ' + str(int(sum(bps_up) / len(bps_up))).rjust(16, ' '))

def applications():
    app_dict = {}
    # Get cached file
    if (os.path.exists(APP_FILE)):
        with open(APP_FILE) as localfile:
            data = json.load(localfile);
    else:
        params = {
            'settings_data_format': 'objects',
            'settings_limit': 5000,
        }
        headers = {
            'content-type': 'application/json',
        }
        response = requests.get(URL, data=json.dumps(params), headers=headers)
        # Save to cache
        with open(APP_FILE, mode='w') as localfile:
            json.dump(response.json()['data'], localfile)
        data = response.json()['data']
    for application in data:
        app_dict[application['id']] = application

    return app_dict

if __name__ == "__main__":
    main()
